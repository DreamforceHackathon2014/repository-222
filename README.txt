Judging Criterion:

Inovation:
-Concept - To deliver a more fluent and efficient tool for Agents using Salesforce.
-Features - Eloquent use of Android watch to create Activities and keep agents up to date with chatter.

User Expereince:
-Our target users are the Agents that use salesforce while on the go.
-Problem solved - We have allowed agents to become much more efficient and aware while out of the offce/traveling.
-UI - in progress.
-Application is centered around information and efficiency, so it is certainly optimized for its clients as well.

Business Potential:
-The target market is every Agent who uses salesforce while traveling, and with the continual growth of Salesfroce, we should
have a strong, thriving product.
-Acquisition of this app is easy. If you are a current user of Salesforce, you can access the app on the Android Play Store.
-Competitors consist of other CRM companies, but we believe that none are employing this cutting edge technology.

Use of SalesForce Technologies:
-Advanced use of chatter and Activity creation through watch and google voice technologies.
-Our application uses REST, API and background processessing to deliver a fast and streamlined expereince.
-Since our app is native, we have a wide variety of API integration with our own custom code, written from scratch during 
this hackathon.
