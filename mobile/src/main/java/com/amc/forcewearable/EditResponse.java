package com.amc.forcewearable;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by Allen on 10/11/2014.
 */
public class EditResponse extends Activity implements View.OnClickListener{

    EditText etSubject, etResponse;
    Button bCancel, bSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_response);
        init();
    }

    private void init(){
        etSubject  = (EditText) findViewById(R.id.etSubject);
        etResponse = (EditText) findViewById(R.id.etResponse);
        bSubmit = (Button) findViewById(R.id.bSubmit);
        //bCancel = (Button) findViewById(R.id.bCancel);

        bSubmit.setOnClickListener(this);
        //bCancel.setOnClickListener(this);

        //SharedPreferences subjects = getSharedPreferences("response subjects", MODE_PRIVATE);
        SharedPreferences descriptions = getSharedPreferences("response descriptions", MODE_PRIVATE);

        //String responseSubjects = subjects.getString("response subjects", "");
        String responseDescription = descriptions.getString("response descriptions", "");
        String subject = getIntent().getStringExtra("response");
        int position = getIntent().getIntExtra("position", 0);

        String description = getDescriptionAt(position, responseDescription);

        etSubject.setText(subject);
        etResponse.setText(description);

    }

    /*private String getDescription(String subject, String responseSubjects, String responseDescription){
        int subjectPosition = 0;

        StringBuffer stringBuffer = new StringBuffer();

        for(char c : responseSubjects.toCharArray()){
            if(c != ','){
                stringBuffer.append(c);
            }else if(stringBuffer.toString().equals(subject)){
                //Log.d("subject", subject);
                Log.d("subject position", "" + subjectPosition);
                Log.d("response descriptions", "" + responseDescription);
                return getDescriptionAt(subjectPosition, responseDescription);
            }else{
                subjectPosition++;
                stringBuffer = new StringBuffer();
            }
        }

        return "getDescription failed";
    }
    */

    private String getDescriptionAt(int subjectPosition, String responseDescriptions){

        int position = 0;

        StringBuffer stringBuffer = new StringBuffer();

        for(char c : responseDescriptions.toCharArray()){
            if(c == ','){
                //Log.d("description", stringBuffer.toString());

                if(position == subjectPosition){
                    return stringBuffer.toString();
                }else{
                    position++;
                    stringBuffer = new StringBuffer();
                }
            }else{
                stringBuffer.append(c);
            }
        }

        return "getDescriptionAt failed";
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.bSubmit){
            deleteResponse();

            SharedPreferences subjects = getSharedPreferences("response subjects", MODE_PRIVATE);
            SharedPreferences descriptions = getSharedPreferences("response descriptions", MODE_PRIVATE);

            SharedPreferences.Editor subjectEditor = subjects.edit();
            SharedPreferences.Editor descriptionsEditor = descriptions.edit();

            String responseSubjects = subjects.getString("response subjects", "");
            String responseDescription = descriptions.getString("response descriptions", "");

            subjectEditor.putString("response subjects", responseSubjects + etSubject.getText().toString() + ",");
            descriptionsEditor.putString("response descriptions", responseDescription + etResponse.getText().toString() + "," );

            subjectEditor.apply();
            descriptionsEditor.apply();

            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_response_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_delete:
                deleteResponse();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void deleteResponse(){
        int deletePosition = getIntent().getIntExtra("position", -1);

        if(deletePosition != -1){
            SharedPreferences subjects = getSharedPreferences("response subjects", MODE_PRIVATE);
            SharedPreferences descriptions = getSharedPreferences("response descriptions", MODE_PRIVATE);

            SharedPreferences.Editor subjectEditor = subjects.edit();
            SharedPreferences.Editor descriptionsEditor = descriptions.edit();

            String responseSubjects = subjects.getString("response subjects", "");
            String responseDescription = descriptions.getString("response descriptions", "");

            responseSubjects = delete(responseSubjects, deletePosition);
            responseDescription = delete(responseDescription, deletePosition);

            subjectEditor.putString("response subjects", responseSubjects);
            descriptionsEditor.putString("response descriptions", responseDescription);

            subjectEditor.apply();
            descriptionsEditor.apply();
        }
    }

    private String delete(String response, int deletePosition){
        int position = 0;
        StringBuffer stringBuffer = new StringBuffer();

        for(char c : response.toCharArray()){
            if(position != deletePosition)stringBuffer.append(c);
            if(c == ',') position++;
        }

        Log.d("response", stringBuffer.toString());
        return stringBuffer.toString();
    }
}
