package com.amc.forcewearable;

/**
 * Created by matthew on 10/10/14.
 */
public interface TaskReceiver {
    public void onTaskComplete(Class taskType, Object data);
}
