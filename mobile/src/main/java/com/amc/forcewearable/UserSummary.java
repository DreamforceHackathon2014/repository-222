package com.amc.forcewearable;

import com.google.gson.annotations.SerializedName;

public class UserSummary
{
    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private String id;

    public String getName() {
        return name;
    }
    public String getId() {
        return id;
    }
}
