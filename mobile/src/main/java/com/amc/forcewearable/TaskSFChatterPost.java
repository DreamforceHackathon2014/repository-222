package com.amc.forcewearable;

import android.os.AsyncTask;
import android.util.Log;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedInput;


/**
 * Created by Matt on 10/11/2014.
 */
public class TaskSFChatterPost extends AsyncTask<Integer,Integer,String> {
    public static void execute(String s){
        (new TaskSFChatterPost(s)).execute();
    }

    static final String TAG = "Post";
    String comment;

    public TaskSFChatterPost(String comment){
        this.comment = comment;
    }

    @Override
    protected String doInBackground(Integer... integers) {
        try {
            Object response = TaskSFLogin.service.chatterPost(comment, "me");
            Log.d(TAG, "Chatter Post Response: " + response);
        }catch(Exception e){
            Log.e(TAG,"Reached hourly limit!");
        }
        return null;
    }
    @Override
    protected void onPostExecute(String response)
    {

    }
}
