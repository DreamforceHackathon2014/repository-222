package com.amc.forcewearable;

import android.os.AsyncTask;
import android.util.Log;

import com.amc.forcewearable.MainActivity;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.List;

/**
 * Created by matthew on 10/10/14.
 */
public class TaskAMCSubmitActivity extends AsyncTask<Integer,Integer,Integer>{
    public static void execute(TaskReceiver receiver, String name, String ID, String URL, long time, String message){
        (new TaskAMCSubmitActivity(receiver, name, ID, URL, time, message)).execute();
    }

    public final static String SOAP_METHOD="CreateActivity";
    Object response;
    String name,ID,message,url;
    long time;
    TaskReceiver receiver;

    public TaskAMCSubmitActivity(TaskReceiver receiver, String name, String ID, String URL, long time, String message){
        this.receiver = receiver;
        this.name = name;
        this.ID = ID;
        this.url = URL;
        this.time = time;
        this.message = message;
    }

    @Override
    protected Integer doInBackground(Integer... integers) {

        SoapObject request = new SoapObject(MainActivity.SOAP_NAMESPACE, SOAP_METHOD);

        SoapObject contact = new SoapObject();
        contact.addProperty("Name", name);
        contact.addProperty("ID", ID);
        contact.addProperty("URL", url);
        contact.addProperty("ConnectionIndicator",false);

        SoapObject displayFields = new SoapObject();

        SoapObject subjectField = new SoapObject();
        subjectField.addProperty("DisplayLabel","Subject");
        subjectField.addProperty("SchemaName"  ,"Subject");
        subjectField.addProperty("FieldValue"  ,"Message From Wearable");

        SoapObject descriptionField = new SoapObject();
        descriptionField.addProperty("DisplayLabel","Description");
        descriptionField.addProperty("SchemaName"  ,"Description");
        descriptionField.addProperty("FieldValue"  ,message);

        SoapObject timeField = new SoapObject();
        timeField.addProperty("DisplayLabel","CallDurationInSeconds");
        timeField.addProperty("SchemaName"  ,"CallDurationInSeconds");
        timeField.addProperty("FieldValue"  ,time);

        displayFields.addProperty("DisplayField",subjectField);
        displayFields.addProperty("DisplayField",descriptionField);
        displayFields.addProperty("DisplayField",timeField);

        contact.addProperty("DisplayFields",displayFields);

        request.addProperty("Contact", contact);

        SoapSerializationEnvelope soapEnv = new SoapSerializationEnvelope(SoapEnvelope.VER12);
        soapEnv.dotNet = true;
        soapEnv.implicitTypes = true;
        soapEnv.setOutputSoapObject(request);

        HttpTransportSE httpse = new HttpTransportSE(MainActivity.SOAP_URL);
        httpse.debug = true;
        try{
            Log.d("CreateActivity","Creating activity...");
            httpse.call(MainActivity.SOAP_NAMESPACE + SOAP_METHOD,soapEnv, MainActivity.defaultHeaders);
            response = soapEnv.getResponse();
            Log.d("CreateActivity",""+httpse.requestDump);
        }catch(Exception e){
            Log.d("CreateActivity", "Error", e);
            Log.d("CreateActivity", "Sent: " + httpse.requestDump);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Integer unused){
        receiver.onTaskComplete(getClass(), response);
    }
}
