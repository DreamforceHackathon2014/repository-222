package com.amc.forcewearable;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Allen on 10/11/2014.
 */
public class ListAdapterCannedResponses extends BaseAdapter {

    ArrayList<String> cannedResponses;
    Context context;

    TextView tvRespose;

    public ListAdapterCannedResponses(Context context, ArrayList<String> cannedResponses){
        this.context = context;
        this.cannedResponses = cannedResponses;
        cannedResponses.add("Create new...");
    }

    @Override
    public int getCount() {
        return cannedResponses.size();
    }

    @Override
    public Object getItem(int position) {
        return cannedResponses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.row_canned_response, parent, false);

        tvRespose = (TextView) row.findViewById(R.id.tvResponse);
        tvRespose.setText(cannedResponses.get(position));

        return row;
    }
}
