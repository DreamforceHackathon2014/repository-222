package com.amc.forcewearable;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import retrofit.Callback;

/**
 * Created by Matt on 10/11/2014.
 */
public class TaskSFLogin extends AsyncTask<Integer,Integer,String> {

    public static String LOGIN_ENDPOINT = "https://login.salesforce.com";
    public static String SERVICE_ENDPOINT = "https://na17.salesforce.com";
    public static String TAG = "Login";
    public static String grantType = "password";
    public static String CONSUMER_KEY = "3MVG9xOCXq4ID1uFuUVMcLpy7vdeZzyvFkwWpNAedWR_QPVlco_IadWKYItMTDUEIgtc7x4DWVvXpQJTpiFAG";
    public static String CONSUMER_SECRET = "6850601849223818087";
    public static String USERNAME = "jenkinsmj2@vcu.edu";
    public static String PASSWORD = "amcamc123HQFH0ubUEWsCo7R59J6elAAQ";

//    public static String CONSUMER_KEY = "3MVG9xOCXq4ID1uExyNEysqEznO5yzfJjg6ZDOWtBFFsrpPxjrjCklpXUtaSwV_MZnycJJshep8UIsSEn5yHl";
//    public static String CONSUMER_SECRET = "4249250313510015248";
//    public static String USERNAME = "batesmatthewj@gmail.com";
//    public static String PASSWORD = "todtod123WfmEwtDgmmbodNvEZUMgDLR1";

    public static ISalesForceRest service;
    static Callback<SFTokenType> cb;
    public static RestAdapter restAdapter;

    @Override
    protected String doInBackground(Integer... integers) {

        restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String s) {
                        Log.d("RestAdapter", s);
                    }
                })
                .setEndpoint(LOGIN_ENDPOINT)
                .build();

        service = restAdapter.create(ISalesForceRest.class);
        cb = new Callback<SFTokenType>() {
            @Override
            public void success(final SFTokenType token, Response response) {
                restAdapter = new RestAdapter.Builder()
                        .setLogLevel(RestAdapter.LogLevel.FULL)
                        .setLog(new RestAdapter.Log() {
                            @Override
                            public void log(String s) {
                                Log.d(TAG, s);
                            }
                        })
                        .setEndpoint(SERVICE_ENDPOINT)
                        .setRequestInterceptor(new RequestInterceptor() {
                            @Override
                            public void intercept(RequestInterceptor.RequestFacade request) {
                                Log.d("TESTING", "Added header "+token.getAccessToken());
                                request.addHeader("Authorization","Bearer " + token.getAccessToken());
                            }
                        })
                        .build();
                service = restAdapter.create(ISalesForceRest.class);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.e("CALLBACK FAILURE", retrofitError.getMessage());
                retrofitError.printStackTrace();
            }
        };

        service.login(grantType, CONSUMER_KEY, CONSUMER_SECRET, USERNAME, PASSWORD, cb);

        return null;
    }
    @Override
    protected void onPostExecute(String response)
    {

    }
}
