package com.amc.forcewearable;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Allen on 10/11/2014.
 */
public class CreateNewResponse extends Activity implements View.OnClickListener{

    EditText etSubject, etResponse;
    Button bCancel, bSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_new_response);
        init();
    }

    private void init(){
        etSubject  = (EditText) findViewById(R.id.etSubject);
        etResponse = (EditText) findViewById(R.id.etResponse);
        bSubmit = (Button) findViewById(R.id.bSubmit);
        //bCancel = (Button) findViewById(R.id.bCancel);

        bSubmit.setOnClickListener(this);
        //bCancel.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.bSubmit){
            SharedPreferences subjects = getSharedPreferences("response subjects", MODE_PRIVATE);
            SharedPreferences descriptions = getSharedPreferences("response descriptions", MODE_PRIVATE);

            SharedPreferences.Editor subjectEditor = subjects.edit();
            SharedPreferences.Editor descriptionsEditor = descriptions.edit();

            String responseSubjects = subjects.getString("response subjects", "");
            String responseDescription = descriptions.getString("response descriptions", "");

            //Log.d("response subjects", responseSubjects + etSubject.getText().toString() + ",");

            subjectEditor.putString("response subjects", responseSubjects + etSubject.getText().toString() + ",");
            descriptionsEditor.putString("response descriptions", responseDescription + etResponse.getText().toString() + "," );

            subjectEditor.apply();
            descriptionsEditor.apply();

            finish();

        }//else if(v.getId() == R.id.bCancel){
           // finish();
        //}
    }
}
