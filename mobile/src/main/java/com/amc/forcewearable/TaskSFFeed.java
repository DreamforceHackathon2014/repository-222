package com.amc.forcewearable;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Matt on 10/11/2014.
 */
public class TaskSFFeed extends AsyncTask<JSONObject, String, List<String>>{
    Context context;
    public TaskSFFeed(Context context)
    {
        this.context = context;
    }
    String TAG = "TaskFeed";
    List<String> feedUpdates;
    String lastUpdate = "2:1413025643000";
    int notificationId = 100;


    @Override
    protected List<String> doInBackground(JSONObject... jsonObjects) {
        feedUpdates = new ArrayList<String>();
        retrofit.Callback<FeedElementPage> cb = new retrofit.Callback<FeedElementPage>() {
            @Override
            public void success(FeedElementPage feedElementPage, Response response) {
                List<FeedItem> elements = feedElementPage.getFeeds();
                lastUpdate = "2:" + System.currentTimeMillis();
                for (FeedItem feedItem : elements)
                {
                    if (feedItem.getText() != null) {
                        Log.i(TAG, feedItem.getText());
                        feedUpdates.add(feedItem.getText());
                    }
                }
                for(String update : feedUpdates)
                {

                    String messageBody = ""; //this is the reply body

                    RemoteInput chatterInput = new RemoteInput.Builder("chatter reply")
                            .setLabel("Create Chatter Reply")
                            .build();

                    Intent chatterIntent = new Intent(context, ChatterReply.class);
                    PendingIntent chatterPendingIntent = PendingIntent.getActivity(context, 0, chatterIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                    Intent viewIntent = new Intent(context, MainActivity.class);
                    PendingIntent viewPendingIntent = PendingIntent.getActivity(context, 0, viewIntent, 0);

                    NotificationCompat.Action chatterAction =
                            new NotificationCompat.Action.Builder(R.drawable.ic_launcher, "Create Activity", chatterPendingIntent).addRemoteInput(chatterInput).build();

                    NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
                    bigStyle.bigText(messageBody);

                    NotificationCompat.Builder notificationBuilder =new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_reply_icon)
                            .setContentTitle("Chatter")
                            .setContentText(update)
                            .setContentIntent(viewPendingIntent)
                            .extend(new NotificationCompat.WearableExtender().addAction(chatterAction))
                            .setGroup("Chatter")
                            .setStyle(bigStyle);

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                    notificationManager.notify(notificationId, notificationBuilder.build());
                    notificationId++;
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.e(TAG, retrofitError.getMessage());
                retrofitError.printStackTrace();
            }
        };
        ISalesForceRest service = TaskSFLogin.service;
        service.getFeeds(lastUpdate, cb);
        return feedUpdates;
    }
}
