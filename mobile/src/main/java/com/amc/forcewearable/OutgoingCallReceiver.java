package com.amc.forcewearable;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by matthew on 10/12/14.
 */
public class OutgoingCallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(OutgoingCallReceiver.class.getSimpleName(), intent.toString());
        String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        PhoneListener.outPhone = phoneNumber;
        //TODO: Handle outgoing call event here
    }
}