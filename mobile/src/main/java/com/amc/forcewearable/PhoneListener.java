package com.amc.forcewearable;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.RemoteInput;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.NotificationCompat.WearableExtender;

import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Allen on 10/10/2014.
 */
public class PhoneListener extends PhoneStateListener implements TaskReceiver{

    public static boolean phoneRinging = false;
    public static boolean receivingCall = false;
    public static long timeReceived = -1;
    public static String outPhone = null;
    Context context;
    String CONTACT_GROUP_KEY = "contact_group_key";
    int notificationId = 01;

    public PhoneListener(Context context){
        this.context = context;
    }

    @Override
    public void onCallStateChanged(int state, String incomingNumber) {
        super.onCallStateChanged(state, incomingNumber);
        switch (state) {
            case TelephonyManager.CALL_STATE_IDLE:
                Log.d("DEBUG", "IDLE");
                Toast.makeText(context, "CALL IDLE", Toast.LENGTH_LONG).show();
                phoneRinging = false;
                if(receivingCall){
                    receivingCall = false;
                    MainActivity.callTime = System.currentTimeMillis() - timeReceived;
                    VoiceCommand.callDurationMap.put(MainActivity.contactID,MainActivity.callTime);
                    outPhone = null;
                }else{
                    MainActivity.callTime = 0;
                }
                break;

            case TelephonyManager.CALL_STATE_OFFHOOK:
                Log.d("DEBUG", "OFFHOOK");
                Toast.makeText(context, "CALL OFFHOOK", Toast.LENGTH_LONG).show();
                phoneRinging = false;
                receivingCall = true;
                timeReceived = System.currentTimeMillis();
                if(outPhone != null){
                    loadContact(outPhone);
                }
                break;

            case TelephonyManager.CALL_STATE_RINGING:
                Log.d("DEBUG", "RINGING " + incomingNumber);
                Toast.makeText(context, "CALL RINGING", Toast.LENGTH_LONG).show();
                loadContact(incomingNumber);
                phoneRinging = true;
                receivingCall = true;
                break;
        }

    }

    public void loadContact(String ani){
        TaskAMCGetContact.execute(this, ani);
    }

    private void sendNotification(){

        // Key for the string that's delivered in the action's intent
        String EXTRA_VOICE_REPLY = "extra_voice_reply";

        String replyLabel = "Create SalesForce Activity";

        SharedPreferences subjects = context.getSharedPreferences("response subjects", context.MODE_PRIVATE);
        String responseSubjects = subjects.getString("response subjects", "");

        ArrayList<String> replyList = new ArrayList<String>();
        StringBuffer stringBuffer = new StringBuffer();

        for(char c : responseSubjects.toCharArray()){
            if(c != ','){
                stringBuffer.append(c);
            }else{
                replyList.add(stringBuffer.toString());
                stringBuffer = new StringBuffer();
            }
        }

        String[] replyChoices = new String[replyList.size()];
        for(int i = 0; i < replyChoices.length; i++){
            replyChoices[i] = ""+replyList.get(i);
        }

        //Create Activity
        RemoteInput remoteInput = new RemoteInput.Builder(EXTRA_VOICE_REPLY)    //extra_voice_reply
                .setLabel(replyLabel)   //Create SalesForce Activity
                .setChoices(replyChoices)
                .build();

        RemoteInput chatterInput = new RemoteInput.Builder("chatter reply")
                .setLabel("Create Chatter Post")
                .build();

        Intent replyIntent = new Intent(context, VoiceCommand.class);
        Intent chatterIntent = new Intent(context, ChatterCommand.class);

        Bundle extras = new Bundle();
        extras.putString("ContactID"  ,MainActivity.contactID);
        extras.putString("ContactName",MainActivity.contactName);
        extras.putString("ContactURL" ,MainActivity.contactURL);
        replyIntent.putExtras(extras);

        PendingIntent replyPendingIntent = PendingIntent.getActivity(context, 0, replyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent chatterPendingIntent = PendingIntent.getActivity(context, 0, chatterIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Action action =
                new NotificationCompat.Action.Builder(R.drawable.ic_reply_icon, "Create Activity", replyPendingIntent).addRemoteInput(remoteInput).build();
        NotificationCompat.Action chatterAction =
                new NotificationCompat.Action.Builder(R.drawable.ic_reply_icon, "Create Chatter Post", chatterPendingIntent).addRemoteInput(chatterInput).build();

        //Open App on phone
        Intent viewIntent = new Intent(context, MainActivity.class);

        PendingIntent viewPendingIntent = PendingIntent.getActivity(context, 0, viewIntent, 0);

        String messageBody = /*"Name: "       + */MainActivity.contactName       + "\n" +
                             /*"Department: " + */MainActivity.contactDepartment + "\n" +
                             /*"EMail: "      + */MainActivity.contactEMail      + "\n" +
                             /*"Phone: "      + */MainActivity.contactANI        + "\n";


        //Send SMS
        Thread sendSMS = new Thread(new Runnable() {
            @Override
            public void run() {

                String messageBody = /*"Name: "       + */MainActivity.contactName       + "\n" +
                             /*"Department: " + */MainActivity.contactDepartment + "\n" +
                             /*"EMail: "      + */MainActivity.contactEMail      + "\n" +
                             /*"Phone: "      + */MainActivity.contactANI        + "\n";

                SmsManager sms = SmsManager.getDefault();
                SharedPreferences sharedPreferences = context.getSharedPreferences("phone number", context.MODE_PRIVATE);
                String phoneNumber = sharedPreferences.getString("phone number", "5402268111");

                Log.d("phoneNumber", phoneNumber);

                VoiceCommand.contactNameMap.put(MainActivity.contactID, MainActivity.contactName);

                sms.sendTextMessage(phoneNumber, null, messageBody, null, null);
            }
        });
        //sendSMS.run();

        //Make Big Style
        NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
        bigStyle.bigText(messageBody);

        ArrayList<NotificationCompat.Action> actions = new ArrayList<NotificationCompat.Action>();
        actions.add(action);
        actions.add(chatterAction);

        NotificationCompat.Builder notificationBuilder =    new NotificationCompat.Builder(context)
                                                            .setSmallIcon(R.drawable.ic_launcher)
                                                            .setContentTitle("Contact Information")
                                                            .setContentText(MainActivity.contactName)
                                                            .setContentIntent(viewPendingIntent)
                                                            //.setGroup(CONTACT_GROUP_KEY)
                                                            .extend(new WearableExtender().addActions(actions))
                                                            .setStyle(bigStyle);

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        // Build the notification and issues it with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());
        notificationId++;

    }

    @Override
    public void onTaskComplete(Class taskType, Object data) {
        if(taskType == TaskAMCGetContact.class){
            try {

                SoapObject response = (SoapObject) data;
                SoapObject contact = (SoapObject) response.getProperty(0);
                Log.d("GetContact", "Response: " + contact);
                MainActivity.contactName = "" + contact.getProperty("Name");
                MainActivity.contactID = "" + contact.getProperty("ID");
                MainActivity.contactURL = "" + contact.getProperty("URL");
                SoapObject fields = (SoapObject) contact.getProperty("DisplayFields");
                for (int i = 0; i < fields.getPropertyCount(); i++) {
                    SoapObject field = (SoapObject) fields.getProperty(i);
                    if (("" + field.getProperty("DisplayLabel")).equals("E-Mail"))
                        MainActivity.contactEMail = "" + field.getProperty("FieldValue");
                    if (("" + field.getProperty("DisplayLabel")).equals("Title"))
                        MainActivity.contactTitle = "" + field.getProperty("FieldValue");
                    if (("" + field.getProperty("DisplayLabel")).equals("Phone"))
                        MainActivity.contactANI = "" + field.getProperty("FieldValue");
                    if (("" + field.getProperty("DisplayLabel")).equals("Department"))
                        MainActivity.contactDepartment = "" + field.getProperty("FieldValue");

                }
                //MainActivity.contactANI  = ""+contact.getProperty("ANI");
                Log.d("GetContacts", "Got contact " + MainActivity.contactName + "(" + MainActivity.contactID + ")");
                sendNotification();
                //displayGlassLiveCard();
            }catch (Exception e){
                Log.d("GetContacts", "Error: " + e);
            }
        }
    }
}




















