package com.amc.forcewearable;

import android.os.AsyncTask;
import android.util.Log;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.List;

/**
 * Created by matthew on 10/10/14.
 */
public class TaskAMCGetContact extends AsyncTask<Integer,Integer,Integer>{
    public static void execute(TaskReceiver receiver, String ani){
        (new TaskAMCGetContact(receiver,ani)).execute();
    }

    public final static String SOAP_METHOD="GetContacts";
    Object response;
    String ani;
    TaskReceiver receiver;

    public TaskAMCGetContact(TaskReceiver receiver, String ani){
        this.receiver = receiver;
        this.ani = ani;
    }

    @Override
    protected Integer doInBackground(Integer... integers) {

        SoapObject request = new SoapObject(MainActivity.SOAP_NAMESPACE, SOAP_METHOD);

        request.addProperty("ANI",ani);

        SoapSerializationEnvelope soapEnv = new SoapSerializationEnvelope(SoapEnvelope.VER12);
        soapEnv.dotNet = true;
        soapEnv.setOutputSoapObject(request);

        HttpTransportSE httpse = new HttpTransportSE(MainActivity.SOAP_URL);
        httpse.debug = true;
        try{
            Log.d("GetContacts","Getting contacts for ani "+ ani);
            httpse.call(MainActivity.SOAP_NAMESPACE + SOAP_METHOD,soapEnv, MainActivity.defaultHeaders);
            response = soapEnv.getResponse();
        }catch(Exception e){
            Log.d("GetContacts", "Error", e);
            Log.d("GetContacts", "Sent: " + httpse.requestDump);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Integer unused){
        receiver.onTaskComplete(getClass(), response);
    }
}
