package com.amc.forcewearable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Matt on 10/11/2014.
 */
public class FeedItem {

    @SerializedName("actor")
    private UserSummary actor;

    @SerializedName("body")
    private FeedItemBody feedItemBody;

    @SerializedName("id")
    private String id;

    public String getText(){return feedItemBody.getText();}
    public String getName(){return actor.getName();}
    public String getUserId(){return actor.getId();}
    public String getFeedId(){return id;}
}