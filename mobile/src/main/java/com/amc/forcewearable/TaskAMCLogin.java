package com.amc.forcewearable;

import android.os.AsyncTask;
import android.util.Log;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.List;

/**
 * Created by matthew on 10/10/14.
 */
public class TaskAMCLogin extends AsyncTask<Integer,Integer,Integer>{
    public static void execute(TaskReceiver receiver, String username, String password, String token){
        (new TaskAMCLogin(receiver,username,password,token)).execute();
    }

    public final static String SOAP_METHOD="Login4Mob";
    Object response;
    String username, password, token;
    TaskReceiver receiver;

    public TaskAMCLogin(TaskReceiver receiver, String username, String password, String token){
        this.receiver = receiver;
        this.username = username;
        this.password = password;
        this.token    = token;
    }

    @Override
    protected Integer doInBackground(Integer... integers) {

        SoapObject request = new SoapObject(MainActivity.SOAP_NAMESPACE, SOAP_METHOD);

        request.addProperty("username",username);
        request.addProperty("password",password + token);

        SoapSerializationEnvelope soapEnv = new SoapSerializationEnvelope(SoapEnvelope.VER12);
        soapEnv.dotNet = true;
        soapEnv.setOutputSoapObject(request);

        HttpTransportSE httpse = new HttpTransportSE(MainActivity.SOAP_URL);
        httpse.debug = true;
        try{
            List<HeaderProperty> headers = httpse.call(MainActivity.SOAP_NAMESPACE + SOAP_METHOD,soapEnv, MainActivity.defaultHeaders);
            response = soapEnv.getResponse();

            for(HeaderProperty o : headers){
                if((""+o.getKey()).equals("Set-Cookie")){
                    HeaderProperty cookieHeader = new HeaderProperty("Cookie",o.getValue());
                    MainActivity.defaultHeaders.add(cookieHeader);
                    Log.d("Login","Added cookie header " + o.getValue());
                }
            }
        }catch(Exception e){
            Log.d("Login", "Error", e);
            Log.d("Login", "Sent: " + httpse.requestDump);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Integer unused){
        receiver.onTaskComplete(getClass(), response);
    }
}
