package com.amc.forcewearable;

import com.google.gson.annotations.SerializedName;

public class FeedItemBody
{
    @SerializedName("text")
    private String text;

    public String getText(){return text;}
}