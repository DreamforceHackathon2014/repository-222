package com.amc.forcewearable;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created by Allen on 10/12/2014.
 */
public class ChatterReply extends Activity implements TaskReceiver {

    TextView tvVoiceMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voice_command);
        tvVoiceMessage = (TextView) findViewById(R.id.tvVoiceCommand);
        String message = getMessageText(getIntent());

        //Needs to be a task to make a chatter reply
        //TaskAMCSubmitActivity.execute(this, MainActivity.contactName, MainActivity.contactID, MainActivity.contactURL, message);

    }

    private String getMessageText(Intent intent) {

        ClipData extra = intent.getClipData();

        Log.d("TAG", "" + extra.getItemCount());    //Found that I only have 1 extra
        ClipData.Item item = extra.getItemAt(0);    //Retreived that extra as a ClipData.Item

        //ClipData.Item can be one of the 3 below types, debugging revealed
        //The RemoteInput is of type Intent

        Log.d("TEXT", "" + item.getText());
        Log.d("URI", "" + item.getUri());
        Log.d("INTENT", "" + item.getIntent());

        //I edited this step multiple times until I discovered that the
        //ClipData.Item intent contained extras, or rather 1 extra, which was another bundle
        //The key for that bundle was "android.remoteinput.resultsData"
        //and the key to get the voice input from wearable notification was EXTRA_VOICE_REPLY which
        //was set in my previous activity that generated the Notification.

        Bundle extras = item.getIntent().getExtras();
        Bundle bundle = extras.getBundle("android.remoteinput.resultsData");

        for (String key : bundle.keySet()) {
            Object value = bundle.get(key);
            Log.d("TAG", String.format("%s %s (%s)", key,
                    value.toString(), value.getClass().getName()));
        }

        tvVoiceMessage.setText(bundle.get("chatter reply").toString());

        return bundle.get("chatter reply").toString();
    }

    @Override
    public void onTaskComplete(Class taskType, Object data) {

    }
}