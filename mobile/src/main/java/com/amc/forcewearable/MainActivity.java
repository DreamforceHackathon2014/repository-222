package com.amc.forcewearable;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.ksoap2.HeaderProperty;
import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Allen on 10/10/2014.
 */
public class MainActivity extends Activity implements View.OnClickListener, TaskReceiver{


    public static final int SERVICE_AMC = 0, SERVICE_FORCE = 1;
    public static int apiService;

    public static final String SOAP_NAMESPACE = "http://www.amctechnology.com/";
    public static final String SOAP_URL       = "http://64.206.243.48/ContactCanvasService/AMCSalesForceService.asmx";

    public static final String DEFAULT_NAME    = "sababneh@amctechnology.com";//"BatesMatthewJ@Gmail.com";
    public static final String DEFAULT_PASS    = "amcamc123";//"todtod123";
    public static final String DEFAULT_TOKEN   = "texAlthtJ9mkfHSDnEvkiOXM";//"WfmEwtDgmmbodNvEZUMgDLR1";
    public static final ArrayList<String> cannedMessages = new ArrayList<String>(100);

    public static List<HeaderProperty> defaultHeaders = new ArrayList<HeaderProperty>(10);
    public static final String[] defaultCannedMessages = {"No Further Action Required","Action Required By Me", "Action Required By Other"};

    //Results from getContact
    public static String contactName, contactID, contactURL, contactANI, contactAccount, contactDepartment, contactTitle, contactEMail;
    public static long callTime = 0;

    RadioButton radioAMC, radioForce;
    EditText etPhoneNumber;

    PhoneListener listener;
    ToggleButton tbService;
    boolean serviceRunning;
    Intent service;
    Button bConfigureResponses;
    TaskSFLogin login;

    public static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        for(String s : defaultCannedMessages){
            cannedMessages.add(s);
        }
        TaskSFLogin login = new TaskSFLogin();
        login.execute();

        bConfigureResponses = (Button) findViewById(R.id.bConfigureResponses);
        bConfigureResponses.setOnClickListener(this);

        tbService = (ToggleButton) findViewById(R.id.tbService);
        tbService.setOnClickListener(this);
        service = new Intent(this, BootService.class);
        startService(service);

        if(serviceRunning()){
            tbService.setChecked(true);
            serviceRunning = true;
        }else{
            tbService.setChecked(false);
            serviceRunning = false;
        }

        TaskAMCLogin.execute(this, DEFAULT_NAME, DEFAULT_PASS, DEFAULT_TOKEN);

    }

    public boolean openSettings(){
        final Dialog settings = new Dialog(this);
        settings.setContentView(R.layout.options);
        settings.setTitle("Settings");
        settings.show();

        etPhoneNumber = (EditText) settings.findViewById(R.id.etPhoneNumber);
        Button bSubmit = (Button) settings.findViewById(R.id.bSubmit);

        bSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("phone number", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("phone number", etPhoneNumber.getText().toString());

                editor.apply();
                settings.hide();
            }
        });

        return true;
    }

    public void editCans(){

        Intent cannedResponsesIntent = new Intent(this, CannedResponses.class);
        startActivity(cannedResponsesIntent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.launch__screen, menu);
        return true;
    }

    private boolean serviceRunning(){

        ActivityManager am = (ActivityManager)this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> rs = am.getRunningServices(500);

        for (int i=0; i<rs.size(); i++) {
            ActivityManager.RunningServiceInfo rsi = rs.get(i);
            Log.i("Service", "Process " + rsi.process + " with component " + rsi.service.getClassName());
            if(rsi.service.getClassName().equals("com.amc.forcewearable.BootService")){
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.action_settings:
                openSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.tbService){
            if (serviceRunning){
                serviceRunning = false;
                stopService(service);
            }else{
                serviceRunning = true;
                service = new Intent(this, BootService.class);
                startService(service);
            }
        }else if(v.getId() == R.id.bConfigureResponses){
            editCans();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if((service != null)&&(serviceRunning))
            stopService(service);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)
        {
            this.moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onTaskComplete(Class taskType, Object data){
        if(taskType == TaskAMCLogin.class){
            if((""+data).equals("true")){
                Log.d("Login","Login successful!");
                Toast.makeText(this, "Login Successful!", Toast.LENGTH_LONG).show();
            }else{
                Log.d("Login","Login failed!");
            }
        }
        if(taskType == TaskAMCSubmitActivity.class){
            Log.d("Create Activity","Response: " + data);
        }
    }
}
