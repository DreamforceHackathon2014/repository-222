package com.amc.forcewearable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeedElementPage
{
    @SerializedName("currentPageUrl")
    private String currentPageUrl;

    @SerializedName("elements")
    private List<FeedItem> elements;

    @SerializedName("isModifiedToken")
    private String isModifiedToken;

    @SerializedName("nextPageToken")
    private String nextPageToken;

    @SerializedName("nextPageUrl")
    private String nextPageUrl;

    @SerializedName("updatesToken")
    private String updatesUrl;

    public List<FeedItem> getFeeds(){return elements;}
    public String getUpdatesUrl(){return updatesUrl;}
}
