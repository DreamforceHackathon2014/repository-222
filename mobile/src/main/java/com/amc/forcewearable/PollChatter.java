package com.amc.forcewearable;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Allen on 10/12/2014.
 */
public class PollChatter extends BroadcastReceiver implements TaskReceiver {

    Context context;
    TaskSFFeed chatterFeed;
    int notificationId = 100;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        chatterFeed = new TaskSFFeed(context);
        chatterFeed.execute();
    }


    @Override
    public void onTaskComplete(Class taskType, Object data) {

        for(String update : chatterFeed.feedUpdates)
        {
            
            String messageBody = ""; //this is the reply body

            RemoteInput chatterInput = new RemoteInput.Builder("chatter reply")
                    .setLabel("Create Chatter Reply")
                    .build();

            Intent chatterIntent = new Intent(context, ChatterReply.class);
            PendingIntent chatterPendingIntent = PendingIntent.getActivity(context, 0, chatterIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent viewIntent = new Intent(context, MainActivity.class);
            PendingIntent viewPendingIntent = PendingIntent.getActivity(context, 0, viewIntent, 0);

            NotificationCompat.Action chatterAction =
                    new NotificationCompat.Action.Builder(R.drawable.ic_launcher, "Create Activity", chatterPendingIntent).addRemoteInput(chatterInput).build();

            NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
            bigStyle.bigText(messageBody);

            NotificationCompat.Builder notificationBuilder =new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_reply_icon)
                    .setContentTitle("Chatter")
                    .setContentText(update)
                    .setContentIntent(viewPendingIntent)
                    .extend(new NotificationCompat.WearableExtender().addAction(chatterAction))
                    .setGroup("Chatter")
                    .setStyle(bigStyle);

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
            notificationManager.notify(notificationId, notificationBuilder.build());
            notificationId++;
        }
    }
}
