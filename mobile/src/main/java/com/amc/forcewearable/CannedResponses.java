package com.amc.forcewearable;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Allen on 10/11/2014.
 */
public class CannedResponses extends ListActivity implements AdapterView.OnItemClickListener{

    ListView list;
    ListAdapterCannedResponses adapter;
    ArrayList<String> cannedResponses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.canned_responses);

        setCannedResponses();
        adapter = new ListAdapterCannedResponses(this, cannedResponses);

        list = getListView();
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Log.d("canned resposes", position + " : " + (list.getCount() - 1));

        if(position != list.getCount() - 1){
            Intent editResponse = new Intent(this, EditResponse.class);
            editResponse.putExtra("response", ((TextView)view.findViewById(R.id.tvResponse)).getText().toString());
            editResponse.putExtra("position", position);
            startActivity(editResponse);
        }else{
            Intent newResponse= new Intent(this, CreateNewResponse.class);
            startActivity(newResponse);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCannedResponses();
        cannedResponses.add("Create new...");
        adapter.cannedResponses = cannedResponses;
        adapter.notifyDataSetChanged();
    }

    private void setCannedResponses(){
        SharedPreferences sharedPreferences = getSharedPreferences("response subjects", MODE_PRIVATE);
        String responses = sharedPreferences.getString("response subjects", "");

        StringBuffer stringBuffer = new StringBuffer();
        cannedResponses = new ArrayList<String>();

        for(char c : responses.toCharArray()){
            if(c != ','){
                stringBuffer.append(c);
            }else{
                cannedResponses.add(stringBuffer.toString());
                stringBuffer = new StringBuffer();
            }
        }
    }
}
