package com.amc.forcewearable;

/**
 * Created by Matt on 10/11/2014.
 */
import android.database.Observable;

import org.json.JSONObject;

import java.util.Date;
import java.util.Map;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface ISalesForceRest {

    @FormUrlEncoded
    @POST("/services/oauth2/token")
    void login(@Field("grant_type") String grantType, @Field("client_id") String CONSUMER_KEY, @Field("client_secret") String CONSUMER_SECRET,
               @Field("username") String USERNAME, @Field("password") String PASSWORD, Callback<SFTokenType> response);

    @FormUrlEncoded
    @POST("/services/data/v32.0/chatter/feed-elements")
    Object chatterPost(@Field("text") String text, @Field("subjectId") String subjectId);

    @GET("/services/data/v32.0/chatter/feeds/company/feed-elements")
    void getFeeds(@Query("updatedSince") String lastUpdate, Callback<FeedElementPage> cb);


}
