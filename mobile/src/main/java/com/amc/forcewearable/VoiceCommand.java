package com.amc.forcewearable;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.HashMap;

/**
 * Created by Allen on 10/10/2014.
 */
public class VoiceCommand extends Activity implements TaskReceiver{

    public static HashMap<String, Long> callDurationMap = new HashMap<String, Long>();
    public static HashMap<String, String> contactNameMap = new HashMap<String, String>();
    public static HashMap<String, String> contactMessageMap = new HashMap<String, String>();

    TextView tvVoiceMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voice_command);
        tvVoiceMessage = (TextView) findViewById(R.id.tvVoiceCommand);
        String message = getMessageText(getIntent());

        Bundle metadata = getIntent().getExtras();
        String userID = "";
        String userName = "";
        String userURL = "";
        try {
            userID = metadata.getString("ContactID");
            userName = metadata.getString("ContactName");
            userURL = metadata.getString("ContactURL");
        }catch(Exception e){
            Log.d("Error","Can't get data from intent");
            userID = MainActivity.contactID;
            userName = MainActivity.contactName;
            userURL = MainActivity.contactURL;
        }
        long time = MainActivity.callTime;
        try{
            time = callDurationMap.get(userID);
        }catch(Exception e){
            Log.d("SubmitActivity","Unable to load stored call time");
        }
        time /= 1000;

        contactMessageMap.put(userID, message);

        Log.d("SubmitActivity", userID + " " + userName + " " + userURL + " " + time);

        TaskAMCSubmitActivity.execute(this, userName, userID, userURL, time, message);
    }

    private String getMessageText(Intent intent){

        ClipData extra = intent.getClipData();

        Log.d("TAG", "" + extra.getItemCount());    //Found that I only have 1 extra
        ClipData.Item item = extra.getItemAt(0);    //Retreived that extra as a ClipData.Item

        //ClipData.Item can be one of the 3 below types, debugging revealed
        //The RemoteInput is of type Intent

        Log.d("TEXT", "" + item.getText());
        Log.d("URI", "" + item.getUri());
        Log.d("INTENT", "" + item.getIntent());

        //I edited this step multiple times until I discovered that the
        //ClipData.Item intent contained extras, or rather 1 extra, which was another bundle
        //The key for that bundle was "android.remoteinput.resultsData"
        //and the key to get the voice input from wearable notification was EXTRA_VOICE_REPLY which
        //was set in my previous activity that generated the Notification.

        Bundle extras = item.getIntent().getExtras();
        Bundle bundle = extras.getBundle("android.remoteinput.resultsData");

        for (String key : bundle.keySet()) {
            Object value = bundle.get(key);
            Log.d("TAG", String.format("%s %s (%s)", key,
                    value.toString(), value.getClass().getName()));
        }

        tvVoiceMessage.setText(bundle.get("extra_voice_reply").toString());

        return bundle.get("extra_voice_reply").toString();
    }

    @Override
    public void onTaskComplete(Class taskType, Object data) {
        if(taskType == TaskAMCSubmitActivity.class){
            Log.d("SubmitActivity","Response: "+data);
            Toast.makeText(getBaseContext(),"Activity Created!",Toast.LENGTH_LONG).show();
            finish();
        }
    }
}
