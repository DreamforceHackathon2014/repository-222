package com.amc.forcewearable;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Allen on 10/10/2014.
 */
public class PhoneReceiver extends BroadcastReceiver {

    TelephonyManager telephony;

    @Override
    public void onReceive(Context context, Intent intent) {
        PhoneListener phoneListener = new PhoneListener(context);
        telephony = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    public void onDestroy() {
        telephony.listen(null, PhoneStateListener.LISTEN_NONE);
    }
}
