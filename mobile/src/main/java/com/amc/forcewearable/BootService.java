package com.amc.forcewearable;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

/**
 * Created by Allen on 10/10/2014.
 */
public class BootService extends Service {

    PhoneReceiver receiver;
    AlarmManager am;
    PendingIntent pendingIntent;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        receiver = new PhoneReceiver();
        Toast.makeText(getApplicationContext(), "Service Started", Toast.LENGTH_LONG).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                Intent intent1 = new Intent(getApplicationContext(), PollChatter.class);
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0,intent1, PendingIntent.FLAG_CANCEL_CURRENT);

                am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),20*1000, pendingIntent);
            }
        }, 4000);


        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        am.cancel(pendingIntent);
        Toast.makeText(getApplicationContext(), "Service Stopped", Toast.LENGTH_LONG).show();
    }
}
