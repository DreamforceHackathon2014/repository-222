package com.amc.forcewearable;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Allen on 10/11/2014.
 */
public class ChatterCommand  extends Activity implements TaskReceiver{

    TextView tvVoiceMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voice_command);
        tvVoiceMessage = (TextView) findViewById(R.id.tvVoiceCommand);
        String message = getMessageText(getIntent());

        String contactID = "";
        String contactName = "";
        try {
            Bundle metadata = getIntent().getExtras();
            contactID = metadata.getString("ContactID");
            contactName = metadata.getString("ContactName");
        } catch (Exception e) {
            Log.d("Error", "Can't get data from intent");
            contactID = MainActivity.contactID;
            contactName = MainActivity.contactName;
        }

        try {

            String prefix = "Activity with " + contactName;
            String send = VoiceCommand.contactMessageMap.get(contactID);
            if (send != null && ("" + send).length() > 0) {
                prefix += "(" + send + ")";
            }
            prefix += ":";

            message = prefix + message;
        }catch (Exception e){}
        Log.d("chatter post", "message: " + message);

        TaskSFChatterPost.execute(message);

        finish();
        //Needs to be a task to make a chatter post
        //TaskAMCSubmitActivity.execute(this, MainActivity.contactName, MainActivity.contactID, MainActivity.contactURL, message);

    }

    private String getMessageText(Intent intent){

        ClipData extra = intent.getClipData();

        Log.d("TAG", "" + extra.getItemCount());    //Found that I only have 1 extra
        ClipData.Item item = extra.getItemAt(0);    //Retreived that extra as a ClipData.Item

        //ClipData.Item can be one of the 3 below types, debugging revealed
        //The RemoteInput is of type Intent

        Log.d("TEXT", "" + item.getText());
        Log.d("URI", "" + item.getUri());
        Log.d("INTENT", "" + item.getIntent());

        //I edited this step multiple times until I discovered that the
        //ClipData.Item intent contained extras, or rather 1 extra, which was another bundle
        //The key for that bundle was "android.remoteinput.resultsData"
        //and the key to get the voice input from wearable notification was EXTRA_VOICE_REPLY which
        //was set in my previous activity that generated the Notification.

        Bundle extras = item.getIntent().getExtras();
        Bundle bundle = extras.getBundle("android.remoteinput.resultsData");

        for (String key : bundle.keySet()) {
            Object value = bundle.get(key);
            Log.d("TAG", String.format("%s %s (%s)", key,
                    value.toString(), value.getClass().getName()));
        }
        String message = bundle.get("chatter reply").toString();
        tvVoiceMessage.setText(message);


        return bundle.get("chatter reply").toString();
    }

    @Override
    public void onTaskComplete(Class taskType, Object data) {

    }
}
