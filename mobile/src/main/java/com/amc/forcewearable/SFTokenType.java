package com.amc.forcewearable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Matt on 10/11/2014.
 */
public class SFTokenType {

    @SerializedName("id")
    private String id;

    @SerializedName("issued_at")
    private String date;

    @SerializedName("token_type")
    private String tokenType;

    @SerializedName("instance_url")
    private String url;

    @SerializedName("signature")
    private String signature;

    @SerializedName("access_token")
    private String accessToken;

    public String getAccessToken() {
        return accessToken;
    }
}
